
add_library(Qt5::UipAssetImporterPlugin MODULE IMPORTED)

set(_Qt5UipAssetImporterPlugin_MODULE_DEPENDENCIES "Quick3DAssetImport;Gui;Core")

foreach(_module_dep ${_Qt5UipAssetImporterPlugin_MODULE_DEPENDENCIES})
    if(NOT Qt5${_module_dep}_FOUND)
        find_package(Qt5${_module_dep}
             ${_Qt5Quick3DAssetImport_FIND_VERSION_EXACT}
            ${_Qt5Quick3DAssetImport_DEPENDENCIES_FIND_QUIET}
            ${_Qt5Quick3DAssetImport_FIND_DEPENDENCIES_REQUIRED}
            PATHS "${CMAKE_CURRENT_LIST_DIR}/.." NO_DEFAULT_PATH
        )
    endif()
endforeach()

_qt5_Quick3DAssetImport_process_prl_file(
    "${_qt5Quick3DAssetImport_install_prefix}/plugins/assetimporters/uip.prl" RELEASE
    _Qt5UipAssetImporterPlugin_STATIC_RELEASE_LIB_DEPENDENCIES
    _Qt5UipAssetImporterPlugin_STATIC_RELEASE_LINK_FLAGS
)

_qt5_Quick3DAssetImport_process_prl_file(
    "${_qt5Quick3DAssetImport_install_prefix}/plugins/assetimporters/uipd.prl" DEBUG
    _Qt5UipAssetImporterPlugin_STATIC_DEBUG_LIB_DEPENDENCIES
    _Qt5UipAssetImporterPlugin_STATIC_DEBUG_LINK_FLAGS
)

set_property(TARGET Qt5::UipAssetImporterPlugin PROPERTY INTERFACE_SOURCES
    "${CMAKE_CURRENT_LIST_DIR}/Qt5Quick3DAssetImport_UipAssetImporterPlugin_Import.cpp"
)

_populate_Quick3DAssetImport_plugin_properties(UipAssetImporterPlugin RELEASE "assetimporters/libuip.a" TRUE)
_populate_Quick3DAssetImport_plugin_properties(UipAssetImporterPlugin DEBUG "assetimporters/libuipd.a" TRUE)

list(APPEND Qt5Quick3DAssetImport_PLUGINS Qt5::UipAssetImporterPlugin)
set_property(TARGET Qt5::Quick3DAssetImport APPEND PROPERTY QT_ALL_PLUGINS_assetimporters Qt5::UipAssetImporterPlugin)
# $<GENEX_EVAL:...> wasn't added until CMake 3.12, so put a version guard around it
if(CMAKE_VERSION VERSION_LESS "3.12")
    set(_manual_plugins_genex "$<TARGET_PROPERTY:QT_PLUGINS>")
    set(_plugin_type_genex "$<TARGET_PROPERTY:QT_PLUGINS_assetimporters>")
    set(_no_plugins_genex "$<TARGET_PROPERTY:QT_NO_PLUGINS>")
else()
    set(_manual_plugins_genex "$<GENEX_EVAL:$<TARGET_PROPERTY:QT_PLUGINS>>")
    set(_plugin_type_genex "$<GENEX_EVAL:$<TARGET_PROPERTY:QT_PLUGINS_assetimporters>>")
    set(_no_plugins_genex "$<GENEX_EVAL:$<TARGET_PROPERTY:QT_NO_PLUGINS>>")
endif()
set(_user_specified_genex
    "$<IN_LIST:Qt5::UipAssetImporterPlugin,${_manual_plugins_genex};${_plugin_type_genex}>"
)
string(CONCAT _plugin_genex
    "$<$<OR:"
        # Add this plugin if it's in the list of manual plugins or plugins for the type
        "${_user_specified_genex},"
        # Add this plugin if the list of plugins for the type is empty, the PLUGIN_EXTENDS
        # is either empty or equal to the module name, and the user hasn't blacklisted it
        "$<AND:"
            "$<STREQUAL:${_plugin_type_genex},>,"
            "$<OR:"
                "$<STREQUAL:$<TARGET_PROPERTY:Qt5::UipAssetImporterPlugin,QT_PLUGIN_EXTENDS>,Qt5::Quick3DAssetImport>,"
                "$<STREQUAL:$<TARGET_PROPERTY:Qt5::UipAssetImporterPlugin,QT_PLUGIN_EXTENDS>,>"
            ">,"
            "$<NOT:$<IN_LIST:Qt5::UipAssetImporterPlugin,${_no_plugins_genex}>>"
        ">"
    ">:Qt5::UipAssetImporterPlugin>"
)
set_property(TARGET Qt5::Quick3DAssetImport APPEND PROPERTY INTERFACE_LINK_LIBRARIES
    ${_plugin_genex}
)
set_property(TARGET Qt5::UipAssetImporterPlugin APPEND PROPERTY INTERFACE_LINK_LIBRARIES
    "Qt5::Quick3DAssetImport;Qt5::Gui;Qt5::Core"
)
set_property(TARGET Qt5::UipAssetImporterPlugin PROPERTY QT_PLUGIN_TYPE "assetimporters")
set_property(TARGET Qt5::UipAssetImporterPlugin PROPERTY QT_PLUGIN_EXTENDS "")
